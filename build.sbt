name := "mmitframework"


version := "0.1-SNAPSHOT"

scalaVersion := "2.12.13"


idePackagePrefix := Some("com.kytheralabs")

val mode = sys.env.getOrElse("EXEC_MODE", "dev")
val devSparkVersion = "2.0.2"
val prodSparkVersion = "3.1.0"

val ArtifactId = "webcrawlerwrapper"

ThisBuild / organization := "com.kytheralabs"
publishConfiguration := publishConfiguration.value.withOverwrite(true)
publishLocalConfiguration := publishLocalConfiguration.value.withOverwrite(true)
libraryDependencies ++= (
  //if (mode != "dev" )
  Seq("org.apache.spark" %% "spark-sql" % "3.1.0",
    "org.apache.spark" %% "spark-core" % "3.1.0")
  /*else
    Nil*/
  )


libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-library" % scalaVersion.value,
  "org.scala-lang" % "scala-compiler" % scalaVersion.value % "scala-tool",
  "org.jsoup" % "jsoup" % "1.13.1",
  "org.apache.httpcomponents" % "httpclient" % "4.5.13",
  "com.google.code.gson" % "gson" % "2.2.4",
  "com.konghq" % "unirest-java" % "3.11.11" % "provided",
  "org.scala-lang.modules" %% "scala-xml" % "1.3.0",
  "org.scalatest" %% "scalatest" % "3.0.8" % Test,
  "org.yaml" % "snakeyaml" % "1.29",
  "io.delta" % "delta-core_2.12" % "1.0.0",
  "com.databricks" %% "dbutils-api" % "0.0.5" % Provided,
  "org.apache.logging.log4j" % "log4j-core" % "2.14.1",
  "org.apache.logging.log4j" % "log4j-api" % "2.14.1"
)
