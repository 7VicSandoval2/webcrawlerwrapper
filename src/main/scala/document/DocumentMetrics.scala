// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package document

import document.MetricsRow
import management.warehouse.Mailgun

import org.apache.spark.sql.{DataFrame, SparkSession}

import java.sql.Timestamp
import scala.math.abs

class DocumentMetrics(spark: SparkSession, crawlId: String) {
  import spark.implicits._

  var previousEntry: DataFrame = spark.sql(s"""SELECT * FROM warehouse.bcf_metrics WHERE crawlId = "${crawlId}" """)
  var data: DataFrame = this.previousEntry

  def save(agentname: String, execution: String, completion: String, durationStr: String, totalFound: Long, pdfsFound: Long,
           previousCount: String, newPdfFounds: Long, pctChange: Float
          ) : Unit = {

    if( this.previousEntry.count() > 0 ) {
      val updQuery = s"""UPDATE warehouse.bcf_metrics SET CompletionDateTime='${completion}', Duration='${durationStr}',
                   TotalDocumentsFound='${totalFound}', DocumentsFound='${pdfsFound}', PreviousCrawlDocumentsFound='${previousCount}',
                   NewDocumentsFound='${newPdfFounds}', ChangePercentage='${pctChange}' WHERE AgentName='${agentname}' AND CrawlId='${crawlId}'"""
      spark.sql(updQuery)
    }
    else {
      /**
       * Crate table if not exists
       */
      this.createTable(spark)

      val metrics = MetricsRow(crawlId, agentname, Timestamp.valueOf(execution), Timestamp.valueOf(completion), durationStr,
        totalFound, pdfsFound, previousCount.toInt, newPdfFounds, pctChange)

      val metricDf = Seq(metrics).toDF()

      this.data = metricDf

      // Write the metrics data to table
      metricDf.write.format("delta").mode("append").saveAsTable("warehouse.bcf_metrics")
    }
  }

  def sendEmail(emailIds: Array[String]): Unit = {

    val row = this.data.first()

    val content = s"""
          Hi,

          Here are the results from the latest ${row.get(0)} BCF run:

          Crawl ID: ${row.get(1)}
          Execution Date: ${row.get(2)}
          Completion Date: ${row.get(3)}
          Duration: ${row.get(4)}

          Total Documents Found: ${row.get(6)}
          Total Documents Found to date: ${row.get(5)}
          Total Documents Found in Previous Crawl: ${row.get(7)}
          Net New Documents Found: ${row.get(8)}
          % Change from previous crawl: ${row.get(9)}
          """

    val email = emailIds.mkString(";")
    Mailgun.sendEmail(email, "info@spicule.co.uk", row.get(0) + " BCF Results", content )
  }


  def createTable(spark: SparkSession): Unit = {
    spark.sql("CREATE TABLE IF NOT EXISTS warehouse.bcf_metrics(AgentName STRING, CrawlId STRING, StartDateTime TIMESTAMP," +
      "CompletionDateTime TIMESTAMP, Duration STRING, TotalDocumentsFound BIGINT, DocumentsFound BIGINT," +
      "PreviousCrawlDocumentsFound BIGINT, NewDocumentsFound BIGINT, ChangePercentage FLOAT ) using DELTA PARTITIONED BY(AgentName)")
  }


  def checkVarianceNotification(prevcount: Double, currcount: Double, triggerPct: Double, notificationSubject: String, notificationMsg: String): Unit ={

    val pct = ((prevcount - currcount)/((prevcount + currcount)/2))*100

    if(abs(pct)>triggerPct){
      Mailgun.sendEmail("fakevariance@spicule.co.uk", "info@spicule.co.uk", "[APOLLO VARIANCE] " + notificationSubject, notificationMsg)
    }
  }

}
