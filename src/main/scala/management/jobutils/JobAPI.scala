// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package management.jobutils

import crawl.databricksapi.templates.Events
import management.GenericLogger
import management.exceptions.FrameworkException
import management.logger.LogHelper

import com.databricks.dbutils_v1.DBUtilsHolder.dbutils
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils
import org.apache.spark.sql.SparkSession

import scala.collection.mutable.ListBuffer
import scala.io.Source

/**
 * Execute jobs via the databricks API.
 */
object JobAPI extends LogHelper {

  val spark: SparkSession = SparkSession.builder().getOrCreate()
  val glogger = new GenericLogger(spark)
  /**
   * Run a job now
   * @param responseString
   * @param environment
   * @return
   */
  def runJob(responseString: String, environment: String = "prod"): String = {
    val split = responseString.split(":")
    var id = split(1)
    id = id.replace("}", "")
    val k =
      s"""{
         |  "job_id": $id,
         |  "notebook_params": {
         |  }
         |}""".stripMargin
    val url = dbutils.secrets.get("crawlconfig", "apiurl")
    val key = dbutils.secrets.get("crawlconfig", "apikey")

    httpPost(k, url + "/api/2.0/jobs/run-now", key)
  }

  /**
   * Run a spark submit job
   * @param sparkVersion
   * @param clusterType
   * @param clusterSize
   * @param crawlid
   * @param environment
   * @param className
   * @param jarPath
   * @param command
   * @param pluginDir
   * @param cpus
   * @param driverMemory
   * @param executorMemory
   * @param zone
   * @return
   */
  def runSparkSubmit(sparkVersion: String, clusterType: String, clusterSize: Int, crawlid: String, environment: String = "prod",
                     className: String, jarPath: String, command: List[String], pluginDir: String,
                     cpus: String, driverMemory: String, executorMemory: String, zone: String = "us-west-2c"): String = {
    var escapedcommand = List[String]()
    command.foreach(x => {
      if (x contains ("\"")) {
        escapedcommand = escapedcommand :+ x.replaceAll("\"", "\\\\\"")
      } else {
        escapedcommand = escapedcommand :+ x
      }
    })
    val drivermem = if(driverMemory == "-1"){
      ""
    } else {
      s""" "--driver-memory","${driverMemory}g", """
    }

    val execmem = if(executorMemory == "-1"){
      ""
    } else{
      s""" "--executor-memory","${executorMemory}g",  """
    }
    val cmd = escapedcommand.mkString(""""""", """","""", """"""")
    val instanceprofile = dbutils.secrets.get("crawlconfig", "instance_profile_arn")
    val j: String =
      s"""{
         |  "name": "${crawlid}",
         |  "new_cluster": {
         |    "spark_version": "${sparkVersion}",
         |    "spark_conf":{"spark.executor.heartbeatInterval": "200000", "spark.network.timeout":"300000", "spark.reducer.maxReqsInFlight":"1", "spark.shuffle.io.retryWait":"60s", "spark.shuffle.io.maxRetries":"10", "spark.locality.wait.node":"0","spark.executor.extraJavaOptions":"-Dpf4j.pluginsDir=${pluginDir}", "spark.task.cpus":"${cpus}"},
         |    "node_type_id": "${clusterType}",
         |    "init_scripts": [{"dbfs":{"destination":"dbfs:/FileStore/KLI/crawlinit.sh"}}],
         |    "aws_attributes": {
         |      "availability": "SPOT_WITH_FALLBACK",
         |      "first_on_demand":1,
         |      "zone_id":"auto",
         |      "instance_profile_arn": "${instanceprofile}"
         |    },
         |    "num_workers": ${clusterSize},
         |    "cluster_log_conf":{ "dbfs" : { "destination" : "dbfs:/FileStore/logs/${crawlid}" } }
         |  },
         |  "email_notifications": {
         |    "on_start": [],
         |    "on_success": [],
         |    "on_failure": [ "notifications@spicule.co.uk"]
         |  },
         |  "spark_submit_task": {
         |  "parameters": [
         |    "--driver-java-options",
         |    "-Dpf4j.pluginsDir=${pluginDir}",
         |    ${drivermem}
         |    ${execmem}
         |    "--class",
         |    "${className}",
         |    "${jarPath}",
         |    ${cmd}
         |  ]
         |}
         |}""".stripMargin

    log.info("SUBMIT COMMAND")
    log.info(j)
    val url = dbutils.secrets.get("crawlconfig", "apiurl")
    val key = dbutils.secrets.get("crawlconfig", "apikey")
    httpPost(j, url + "/api/2.0/jobs/runs/submit", key)

  }


  /**
   * Post the data to databricks
   * @param json
   * @param url
   * @param key
   * @return
   */
  def httpPost(json: String, url: String, key: String): String = {
    val entity1 = new StringEntity(json);
    val client = HttpClients.createDefault();
    val httpPost = new HttpPost(url);
    log.info("Posting job to: " + url)
    httpPost.addHeader("Authorization", "Bearer " + key)
    httpPost.setEntity(entity1);
    httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    val response1 = client.execute(httpPost);
    val e = response1.getEntity
    val str = EntityUtils.toString(e, "UTF-8")
    log.info("POST RESPONSE: " + str)
    if(response1.getStatusLine.getStatusCode >299){
      throw new Exception(response1.getStatusLine.getStatusCode.toString)
    }
    str
  }

  def fileToCleanList(crawlid: String): String = {
    Source.fromFile("/dbfs/FileStore/crawltokenlists/crawl_" + crawlid).getLines.mkString
  }

  /**
   * Create a new sparksubmit task
   * @param name
   * @param crawlid
   * @param clusterDef
   * @param pluginDir
   * @param className
   * @param jarPath
   * @param command
   * @param drivermemory
   * @param executormemory
   * @param dependson
   * @return
   */
  def createSparkSubmitTask(name: String, crawlid: String, clusterDef: String, pluginDir: String, className: String,
                            jarPath: String, command: List[String], drivermemory: String, executormemory: String, dependson: String): String = {
    val depends = if(dependson != null && dependson != ""){
      s"""
      "depends_on": [
      {
        "task_key": "${dependson}"
      }
      ],
      """
    } else{
      ""
    }
    val drivermem = if(drivermemory == "-1"){
      ""
    } else {
      s""" "--driver-memory","${drivermemory}g", """
    }

    val execmem = if(executormemory == "-1"){
      ""
    } else{
      s""" "--executor-memory","${executormemory}g",  """
    }
    var escapedcommand = List[String]()
    command.foreach(x => {
      if (x contains ("\"")) {
        escapedcommand = escapedcommand :+ x.replaceAll("\"", "\\\\\"")
      } else {
        escapedcommand = escapedcommand :+ x
      }
    })
    val cmd = escapedcommand.mkString(""""""", """","""", """"""")
      s"""{
         |        "task_key": "${crawlid}-${name}",
         |        "description": "Crawl the websites",
         |        ${depends}
         |        ${clusterDef},
         |        "spark_submit_task": {
         |          "parameters": [
         |             "--driver-java-options",
         |             "-Dpf4j.pluginsDir=${pluginDir}",
         |             ${drivermem}
         |             ${execmem}
         |             "--class",
         |             "${className}",
         |             "${jarPath}",
         |             ${cmd}
         |           ]
         |         },
         |        "max_retries": 3,
         |        "min_retry_interval_millis": 0,
         |        "retry_on_timeout": false,
         |        "timeout_seconds": 43200,
         |        "email_notifications": {
         |            "on_start": [],
         |            "on_success": [],
         |            "on_failure": [ "notifications@spicule.co.uk"]
         |        }
         |      }""".stripMargin
  }

  /**
   * Create a new pipeline task
   * @param notebook
   * @param clusterDef
   * @param dependson
   * @param crawlid
   * @param params
   * @return
   */
  def createPipelineTask(notebook: String, clusterDef: String, dependson: String, crawlid: String, params: String = "", taskname: String ="") : String = {
    val tn = if(taskname == ""){
      "pipeline_task"
    } else{
      taskname
    }
    val baseparams = if(params != null && params != ""){
      s"""
      "base_parameters": $params
      """
    } else {
      s"""
      "base_parameters":  {
        "crawlid": "${crawlid}"
      }
      """
    }
    val depends = if(dependson != null && dependson != ""){
      s"""
      "depends_on": [
      {
        "task_key": "${dependson}"
      }
      ],
      """
    } else{
      ""
    }
    s"""
       |{
       |        "task_key": "${tn}",
       |        "description": "Run a pipeline task",
       |        ${depends}
       |        "notebook_task": {
       |          "notebook_path": "${notebook}",
       |          ${baseparams}
       |        },
       |        ${clusterDef},
       |        "max_retries": 3,
       |        "min_retry_interval_millis": 0,
       |        "retry_on_timeout": false,
       |        "timeout_seconds": 36000,
       |        "email_notifications": {
       |             "on_start": [],
       |             "on_success": [],
       |             "on_failure": [ "notifications@spicule.co.uk"]
       |           }
       |      }
       |""".stripMargin
  }

  /**
   * Join tasks together
   * @param tasks
   * @return
   */
  def joinTasks(tasks: List[String]) : String = {
    val t = tasks.mkString(",")
    "[" + t + "]"
  }

  /**
   * Create a multijob request.
   * @param tasks
   * @param jobname
   * @return
   */
  def createMultiJobRequest(tasks: String, jobname: String, crawlid: String, schedule: String): String = {
    val sched = if(schedule != ""){
      s"""
         |"schedule": {
         |
         |    "quartz_cron_expression": "${schedule}",
         |    "timezone_id": "EST",
         |    "pause_status": "UNPAUSED"
         |
         |},
         |""".stripMargin
    } else{
      ""
    }
    val req =
      s"""{
         |    "name": "${jobname}",
         |    "max_concurrent_runs": 1,
         |    "tasks": ${tasks},
         |    ${sched}
         |   "access_control_list": [
         |{
         | "group_name": "users",
         | "permission_level": "CAN_MANAGE"
         |}
         |]
         |}
         |""".stripMargin
    log.info("REQUEST:")
    log.info(req)
    val url = dbutils.secrets.get("crawlconfig", "apiurl")
    val key = dbutils.secrets.get("crawlconfig", "apikey")
    try{
      val resp = httpPost(req, url + "/api/2.1/jobs/create", key)
      if(sched == null || sched == "") {
        runJob(resp)
      } else{
        "Scheduled"
      }
    } catch {
      case e : Exception => {
        glogger.logCrawlEnd(crawlid, success = false)
        glogger.logAdditionalCrawlEvent(crawlid, "Crawl Failed", "Cluster failed to start for crawl")
        throw FrameworkException("Cluster failed to start for crawl " + crawlid, e)
      }
    }


  }

  def listToNotebookTask(l: List[String], clusterDef: String): String ={
    var notebook = ""
    var params = ""
    var dependson = ""
    var name = ""
    l.foreach(o =>{
      if(o.startsWith("type:")){
        name = o.split(":", 2)(1)
      } else if(o.startsWith("notebook:")){
        notebook = l(1).replace("notebook:", "")
      } else if (o.startsWith("dependson:")){
        dependson = o.split(":", 2)(1)
      }else{
        val s = o.split(":", 2)
        params = params + s""" "${s(0)}": "${s(1)}","""
      }
    })
    params = "{" + params.dropRight(1) + "}"
    JobAPI.createPipelineTask(notebook, clusterDef, dependson, crawlid="", params=params, taskname = name)
  }

  /**
   * Generate the post request for the tasks.
   * @param crawlid
   * @param tasks
   * @param pluginDir
   * @param clustersize
   * @param sparklerVersion
   * @param clustertype
   * @param sparkversion
   * @param cpus
   * @param drivermemory
   * @param executormemory
   * @param zone
   * @param transformTemplate
   * @return
   */
  def generateTasksPost(crawlid: String, tasks: Map[String,List[String]], pluginDir: String,
                                clustersize: Map[String, Int], sparklerVersion: String, clustertype: String,
                        sparkversion: String, cpus: String, drivermemory: String, executormemory: String,
                        zone: String = "us-west-2c", transformTemplate: Events, tformlibraries: String = "", schedule : String = "", idname: String = ""): String = {
    val filepath = if(idname.isEmpty){
      crawlid
    } else{
      idname
    }
    val tlist = new ListBuffer[String]()
    var prev: String = null
    val instanceprofile = dbutils.secrets.get("crawlconfig", "instance_profile_arn")
    tasks.foreach(v => {
      println("New task found: "+v._2.head)
      if(v._2.head.contains("type:notebook")){
        println("creating notebook task")
        val clusterdef =
          s"""
             |    "new_cluster": {
             |        "spark_version": "${sparkversion}",
             |        "spark_conf":{"spark.executor.heartbeatInterval": "200000", "spark.network.timeout":"300000", "spark.reducer.maxReqsInFlight":"1", "spark.shuffle.io.retryWait":"60s", "spark.shuffle.io.maxRetries":"10", "spark.locality.wait.node":"0","spark.executor.extraJavaOptions":"-Dpf4j.pluginsDir=${pluginDir} -Dlog4j.configuration=/dbfs/FileStore/release/log4j2.properties -Dlog4j.debug=true", "spark.task.cpus":"${cpus}"},
             |        "node_type_id": "${clustertype}",
             |        "init_scripts": [{"dbfs":{"destination":"dbfs:/FileStore/KLI/crawlinit.sh"}}],
             |        "aws_attributes": {
             |          "availability": "SPOT_WITH_FALLBACK",
             |          "first_on_demand":1,
             |          "zone_id":"auto",
             |          "instance_profile_arn": "${instanceprofile}"
             |        },
             |        "num_workers": 1,
             |        "cluster_log_conf":{ "dbfs" : { "destination" : "dbfs:/FileStore/logs/${crawlid}" } }
             |        }
             |""".stripMargin
        val task = listToNotebookTask(v._2, clusterdef)
        prev = v._2.head.split(":", 2)(1)
        tlist += task
      } else {
        println("creating submit task")
        val size = clustersize(v._1)
        val clusterdef =
          s"""
             |    "new_cluster": {
             |        "spark_version": "${sparkversion}",
             |        "spark_conf":{"spark.executor.heartbeatInterval": "200000", "spark.network.timeout":"300000", "spark.reducer.maxReqsInFlight":"1", "spark.shuffle.io.retryWait":"60s", "spark.shuffle.io.maxRetries":"10", "spark.locality.wait.node":"0","spark.executor.extraJavaOptions":"-Dpf4j.pluginsDir=${pluginDir} -Dlog4j.configuration=/dbfs/FileStore/release/log4j2.properties -Dlog4j.debug=true", "spark.task.cpus":"${cpus}"},
             |        "node_type_id": "${clustertype}",
             |        "init_scripts": [{"dbfs":{"destination":"dbfs:/FileStore/KLI/crawlinit.sh"}}],
             |        "aws_attributes": {
             |          "availability": "SPOT_WITH_FALLBACK",
             |          "first_on_demand":1,
             |          "zone_id":"auto",
             |          "instance_profile_arn": "${instanceprofile}"
             |        },
             |        "num_workers": ${size},
             |        "cluster_log_conf":{ "dbfs" : { "destination" : "dbfs:/FileStore/logs/${crawlid}" } }
             |        }
             |""".stripMargin
        val task = JobAPI.createSparkSubmitTask(v._1, crawlid, clusterdef,
          pluginDir, "edu.usc.irds.sparkler.Main",
          "dbfs:/FileStore/release/sparkler-app-" + sparklerVersion + ".jar", v._2, drivermemory, executormemory, prev)
        prev = crawlid + "-" + v._1
        tlist += task
      }
    })

    val tlibs = if(tformlibraries == "") {
      val s = Source.fromFile("/dbfs/FileStore/crawl/config/defaultdependencies.json")
      val str = s.mkString
      s.close()
      str
    } else{
      tformlibraries
    }

    val cluster = s""""new_cluster": {
                    |    "spark_version": "8.3.x-scala2.12",
                    |    "spark_conf": {
                    |        "spark.serializer": "org.apache.spark.serializer.KryoSerializer",
                    |        "spark.kryoserializer.buffer.max": "2000M"
                    |    },
                    |    "node_type_id": "i3.xlarge",
                    |    "aws_attributes": {
                    |      "availability": "ON_DEMAND",
                    |      "instance_profile_arn": "${instanceprofile}"
                    |    },
                    |    "num_workers": 3
                    |  },
                    |  $tlibs
                    |  """.stripMargin
    import scala.collection.JavaConverters._
    val params = transformTemplate.getShutdown.getTriggerjob.getParameters.asScala

    var strvals = ""
    params.foreach(k => {
      val s = s""" "${k._1}" : "${k._2}", """
      strvals += s

    })
    strvals = strvals.trim().dropRight(1)
    val jsonParams = "{" +strvals + "}"
    val transform = createPipelineTask(transformTemplate.getShutdown.getTriggerjob.getNotebook, cluster, prev, filepath, jsonParams)
    tlist += transform
    val jt = JobAPI.joinTasks(tlist.toList)
    val jobname = if(schedule != null && schedule != "" ){
      "scheduledcrawl_"+crawlid
    } else{
      "formularycrawl_"+crawlid
    }
    JobAPI.createMultiJobRequest(jt, jobname, crawlid, schedule)
  }




}
