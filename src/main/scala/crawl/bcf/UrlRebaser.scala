// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.
package com.kytheralabs
package crawl.bcf

import crawl.Helper

import com.google.gson.JsonParser
import org.apache.commons.io.FilenameUtils
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.util.EntityUtils
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.io.{BufferedWriter, File, FileWriter}
import java.net.{URL, URLEncoder}
import java.nio.charset.StandardCharsets
import scala.collection.mutable.ListBuffer

class UrlRebaser(spark: SparkSession) {
  import spark.implicits._
  val httpClient: HttpClient = HttpClientBuilder.create().build()

  var l : List[String]  = List[String]()

  def fetchNextLinks(dfDocs: DataFrame): Unit = {

    l = l ::: dfDocs.select("url").map(r => r.getString(0)).collect.toList
    val fruits = new ListBuffer[String]()
    dfDocs.select($"outlinks"(0)).collect.toList.foreach(i =>{
      if(i(0)!= null){
        if(i(0).toString.endsWith(".html") || i(0).toString.endsWith(".htm") || i(0).toString.endsWith(".aspx")|| i(0).toString.endsWith(".asp")){
          fruits += i(0).toString
        }
        val url = new URL(i(0).toString);
        if(FilenameUtils.getExtension(url.getPath) == ""){
          fruits += i(0).toString
        }
      }
    })
    l = l ::: fruits.toList
  }

  def grabresults(crawlId: String): List[String] = {
    val crawlSolrDbUrl = "http://ec2-35-174-200-133.compute-1.amazonaws.com:8983/solr/crawldb"


    var urlencode = URLEncoder.encode("content_type:text/html AND crawl_id:" + crawlId )
    val u1 = "/select?&rows=0&q="+urlencode
    println(u1)
    val get = new HttpGet(crawlSolrDbUrl+u1)
    val response = httpClient.execute(get)
    val entity = response.getEntity
    val totalCountJson = EntityUtils.toString(entity, StandardCharsets.UTF_8)
    val totalCountObj = new JsonParser().parse(totalCountJson).getAsJsonObject
    val totalCount = totalCountObj.get("response").getAsJsonObject.get("numFound").getAsLong


    var recordsRetrieved = 0
    while (recordsRetrieved < totalCount) {
      urlencode = URLEncoder.encode("content_type:text/html AND crawl_id:" + crawlId )
      println("URL to query: "+"/select?fl=url,outlinks&rows=100&sort=indexed_at%20asc&start=" + recordsRetrieved+"&q=content_type:text/html AND crawl_id:" + crawlId)
      val get = new HttpGet(crawlSolrDbUrl+"/select?&rows=100&sort=indexed_at%20asc&start=" + recordsRetrieved+"&q="+urlencode)
      val response = httpClient.execute(get)
      val entity = response.getEntity
      val totalCountJson = EntityUtils.toString(entity, StandardCharsets.UTF_8)
      val result = new JsonParser().parse(totalCountJson).getAsJsonObject
      val inputJson = result.get("response").getAsJsonObject.get("docs")
      val batch = spark.createDataset(inputJson.toString :: Nil)
      val j = spark.read.json(batch)
      val dfDocs = Helper.cleanDataFrame(j)

      fetchNextLinks(dfDocs)

      println("recordsRetrieved=" + recordsRetrieved + " : dfDocs.count=" + dfDocs.count.toInt)
      recordsRetrieved = recordsRetrieved + dfDocs.count.toInt
    }
    l
  }

  def writeFile(filename: String, lines: Seq[String]): Unit = {
    val file = new File(filename)
    val bw = new BufferedWriter(new FileWriter(file))
    for (line <- lines) {
      bw.write(line)
    }
    bw.close()
  }

  def writeTempFile(lines: Seq[String]): String = {
    import java.io.File
    val tempFile = File.createTempFile("tokens-", null)
    val bw = new BufferedWriter(new FileWriter(tempFile))
    for (line <- lines) {
      bw.write(line)
    }
    bw.close()
    tempFile.toString
  }


}
