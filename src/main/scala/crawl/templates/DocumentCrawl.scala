// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl.templates

import crawl.bcf.{Profiles, UrlRebaser}
import crawl.databricksapi.templates.Events
import crawl.{AutoDiscover, Persistence, Utils, WebCrawler}
import management.jobutils.{JobAPI, TasksHolder}
import management.{Crawl, JavaCrawl, ManagementCore}

import com.databricks.dbutils_v1.DBUtilsHolder.dbutils
import com.google.gson.Gson
import org.apache.spark.sql.SparkSession

import java.nio.charset.StandardCharsets
import java.time.format.DateTimeFormatter
import java.time.{LocalDateTime, ZoneId}
import java.util.Base64
import scala.collection.JavaConverters._
import scala.collection.convert.ImplicitConversions.`map AsScala`

/**
 * Crawl a domain using a document crawl.
 * @param spark the spark object from the notebook
 * @param sparklerVersion the version of sparkler required
 */

class DocumentCrawl(spark: SparkSession, agentName: String, sparklerVersion : String = "0.5.14", schedule : String = "") extends CoreTemplate(spark = spark, sparklerVersion = sparklerVersion) {

    import spark.implicits._
    val clustersize = 10
    val defaultiterations = 50
    var availableparams: Map[String, String] = Map[String, String]()

    /**
     * Define the default notebook parameters.
     */
    def defaultNotebookParams(): Unit = {
      dbutils.widgets.text("crawlid", "")
      dbutils.widgets.text("formularyid", "")
      dbutils.widgets.text("agentname", "")
      dbutils.widgets.dropdown("executiontype", "Development", Seq("Development", "Production"), "Execution Type")
    }

    /**
     * Get the parameters.
     *
     * @return
     */
    override def getParams: Map[String, String] = {
      println("Looking up required params")
      Map("crawlid" -> dbutils.widgets.get("crawlid"), "agentname" -> dbutils.widgets.get("agentname"), "executiontype" -> dbutils.widgets.get("executiontype"),
        "formularyid" -> dbutils.widgets.get("formularyid"), "idname" -> dbutils.widgets.get("idname"))
    }

    /**
     * Generate a crawl id.
     */
    def createCrawlId(): Unit = {
      val fmt = DateTimeFormatter.ofPattern("uuuuMMddHHmmss")
      val time = LocalDateTime.now(ZoneId.of("UTC")).format(fmt)
      val crawlid = availableparams("agentname") + time
      availableparams = availableparams + ("crawlid" -> crawlid)
    }

    /**
     * Check the required parameters are set.
     */
    def checkParams(): Unit = {
      if (availableparams("agentname") == "") {
        throw new Exception("No agent name set")
      }
      if (availableparams("crawlid") == "") {
        createCrawlId()
      }
    }

    /**
     * Stage the crawl urls
     *
     * @param url             a list of urls
     * @param availableparams the supplied parameters
     * @param profiles        the required profiles
     */
    def stageCrawl(url: List[String], availableparams: Map[String, String], profiles: Map[String, String]): Seq[String] = {
      val inputDataframeOfURLS = url.toDF("DOMAIN")
      //This is development mode, production needs to do this in sparksubmit
      val webCrawler = new WebCrawler("local", true, sparklerPath = sparklerpath)
      webCrawler.removeUrlInjector()
      val idfile = if(schedule != ""){
        "/dbfs/FileStore/seeds/doccrawl_id_"+availableparams("idname")
      }  else{
        ""
      }
      webCrawler.getInjectArgs(availableparams("crawlid"), inputDataframeOfURLS.select('DOMAIN),
        "/dbfs/FileStore/seeds/" + availableparams("crawlid") + "seeds.txt", idfile = idfile)
    }

    /**
     * Run the crawl.
     *
     * @param url          a list of urls
     * @param autodiscover whether to suppliment the urls with more discovered urls
     * @param profiles     the required profiles
     * @param injectonly   whether to inject only or run a full crawl
     * @param runonly      whether to skip the inject phase and run an already staged crawl
     * @param iterations   how many iterations to run
     * @param clustersize  the size of the cluster required
     * @return
     */
    def runCrawl(url: List[String], autodiscover: Boolean, profiles: Map[String, String], injectonly: Boolean = false,
                 runonly: Boolean = false, iterations: Int = defaultiterations, clustersize: Int = 1): String = {

      availableparams = getParams
      checkParams()

      var l = url
      if (autodiscover) {
        val ad = new AutoDiscover
        l = l ::: ad.findSiteMap(url.head)
        l = l ::: ad.scrapeDuckDuckGo(url.head)
      }
      l = l.distinct

      val u = new Utils
      u.writeFile("/dbfs/FileStore/seeds/" + availableparams("crawlid") + "seeds.txt", l)

      //TODO Re-Crawl Logic
      val urlrebaser = new UrlRebaser(spark)
      val urls = urlrebaser.grabresults(availableparams("crawlid"))
      //If crawl exists, run pre-seed on new crawl
      var inject: Seq[String] = null
      if (urls.nonEmpty) {
        log.info("Seeding with previous results")
        val p = new Persistence
        p.purgeCrawlFromSolr(availableparams("crawlid"))
        inject = stageCrawl(urls, availableparams, profiles)
      } else if (!runonly) {
        inject = stageCrawl(l, availableparams, profiles)
      }
      if (!injectonly) {
        //TODO need to run infinite iterations
        val m = new ManagementCore(spark)

        val crawldir = "DocumentCrawls"
        val c = if (crawldir == "Crawls" || crawldir == "") {
          m.getCrawl(agentName + "_" + dbutils.widgets.get("formularyid"))
        } else if (crawldir == "DocumentCrawls"){
          m.getDocCrawl(agentName + "_" + dbutils.widgets.get("formularyid"))
        } else {
          m.getJCrawl(agentName + "_" + dbutils.widgets.get("formularyid"))
        }
        executeCrawl(availableparams, iterations, clustersize, profiles, Some(inject), c, crawldir)
        "Running Full Crawl"
      } else {
        "Inject Only Job"
      }
    }

  private def runDevelopmentCrawl(profiles: Map[String,String]): String ={
    var webCrawler = new WebCrawler("local", true, sparklerPath = sparklerpath)
    webCrawler.buildConfigurationJson()
    webCrawler.setContentFileName()
    webCrawler.setContentPath("/dbfs/FileStore/crawl/content/")
    val p = new Profiles(webCrawler)
    webCrawler = p.applyProfiles(profiles)

    println("Launching in development mode")
    val i = webCrawler.crawl(availableparams("crawlid"))
    i.toString
  }

  private def runProductionCrawl(availableparams: Map[String, String], iterations: Int, clustersize: Int = clustersize,
                                 profiles: Map[String, String], injectargs: Option[Seq[String]], c: Option[Crawl], crawldir: String) : String = {
    //run spark submit
    log.info("Launching in production mode")
    var webCrawler = new WebCrawler("local", false, sparklerPath = sparklerpath)
    webCrawler.buildConfigurationJson("production")
    val e = generateTransformTemplate(availableparams, c, crawldir)

    n.logCrawlStart(availableparams("crawlid"), null, "c5d.4xlarge", 3, 0, 0)

    webCrawler.waitForJavascript(true)

    val partitionCount = 250
    webCrawler.setRepartitionCount(partitionCount)
    webCrawler.setContentPath("/dbfs/FileStore/crawl/content/")

    val p = new Profiles(webCrawler)
    webCrawler = p.applyProfiles(profiles)

    webCrawler.crawlDepth = iterations
    webCrawler.setSparkMaster("")
    val args = webCrawler.getCrawlArgs(availableparams("crawlid"), "production", None)

    val tasks: Map[String, java.util.List[String]] = if (injectargs.isDefined) {
      Map("inject" -> injectargs.get.toList.asJava, "crawl" -> args.toList.asJava)
    } else {
      Map("crawl" -> args.toList.asJava)
    }

    val clustersize: Map[String, String] = if (injectargs.isDefined) {
      Map("inject" -> "1", "crawl" -> "3")
    } else {
      Map("crawl" -> "3")
    }

    log.info("Calling submit job")


    val formulary = c.get.config("formularyid")

    val gson = new Gson()
    val jcrawl = JavaCrawl(c.get.name, c.get.path, c.get.config.asJava, c.get.transformPath.get, c.get.state)
    val task = TasksHolder(availableparams("crawlid"), formulary, tasks.asJava, getPluginDir, clustersize.asJava, getSparklerVersion, jcrawl, e)

    val tson = gson.toJson(task)
    val cid = availableparams("crawlid")
    val ts = LocalDateTime.now()
    val encode = Base64.getEncoder.encodeToString(tson.getBytes(StandardCharsets.UTF_8))
    spark.sql(s"""insert into warehouse.crawl_queue values("$agentName", "$cid", "$encode", "$ts", "$formulary")""")
    log.info("crawl queued")
    "Queued"
  }

    /**
     * Execute a crawl.
     *
     * @param availableparams the supplied parameters
     * @param iterations      the number of iterations required
     * @param clustersize     the cluster size required
     * @param profiles        the profiles required
     * @return
     */
    private def executeCrawl(availableparams: Map[String, String], iterations: Int, clustersize: Int = clustersize,
                     profiles: Map[String, String], injectargs: Option[Seq[String]], c: Option[Crawl], crawldir: String): String = {
      println("Running Crawl")
      if (availableparams("executiontype") == "Development") {
        runDevelopmentCrawl(profiles)
      } else {
        runProductionCrawl(availableparams, iterations, clustersize, profiles, injectargs, c, crawldir)
        //run spark submit
        println("Launching in production mode")
        var webCrawler = new WebCrawler("local", false, sparklerPath = sparklerpath)
        webCrawler.buildConfigurationJson("production")
        val e = generateTransformTemplate(availableparams, c, crawldir)

        n.logCrawlStart(availableparams("crawlid"), null, "c5d.4xlarge", 3, 0, 0)

        webCrawler.waitForJavascript(true)

        val partitionCount = 25
        webCrawler.setRepartitionCount(partitionCount)
        webCrawler.setContentPath("/dbfs/FileStore/crawl/content/")

        val p = new Profiles(webCrawler)
        webCrawler = p.applyProfiles(profiles)

        webCrawler.crawlDepth = iterations
        webCrawler.setSparkMaster("")
        val idfile = if(schedule != ""){
          "/dbfs/FileStore/seeds/doccrawl_id_"+availableparams("idname")

        }  else{
          ""
        }
        if(schedule != ""){
          e.getShutdown.getTriggerjob.getParameters("crawlid") = "/dbfs/FileStore/seeds/doccrawl_id_"+availableparams("idname")
        }
        val args = webCrawler.getCrawlArgs(availableparams("crawlid"), "production", None, idfile = idfile)

        println("Calling submit job")
        var sparklermainver = sparklerVersion
        if (sparklerVersion.contains("-")) {
          val splitpos = sparklerVersion.lastIndexOf("-")
          sparklermainver = sparklerVersion.substring(0, splitpos)
        }
        val pluginDir = "/dbfs/FileStore/release/plugins/plugins-" + sparklermainver


        val formulary = c.get.config("formularyid")

        if(schedule == null || schedule == ""){
          onetimequeue(e, c, availableparams, formulary, pluginDir, injectargs, args, "3")
        } else{
          cronSchedule(e, c, availableparams, injectargs, args, pluginDir, 3, schedule)
        }
      }
    }

  def onetimequeue(e :Events, c: Option[Crawl], availableparams: Map[String,String], formulary: String,
                   pluginDir: String, injectargs: Option[Seq[String]], args: Seq[String], crawlsize: String): String ={
    val l = List("type:notebook", "notebook:/Shared/System/jobidgenerator", "crawlagent:" + availableparams("idname")).asJava
    val tasks: Map[String, java.util.List[String]] = if (injectargs.isDefined) {
      Map("setup" -> l,"inject" -> injectargs.get.toList.asJava, "crawl" -> args.toList.asJava)
    } else {
      Map("setup" -> l, "crawl" -> args.toList.asJava)
    }
    val clustersize: Map[String, String] = if (injectargs.isDefined) {
      Map("setup" -> "1","inject" -> "1", "crawl" -> crawlsize)
    } else {
      Map("setup" -> "1","crawl" -> crawlsize)
    }
    val gson = new Gson()
    val jcrawl = JavaCrawl(c.get.name, c.get.path, c.get.config.asJava, c.get.transformPath.get, c.get.state)
    val task = TasksHolder(availableparams("crawlid"), formulary, tasks.asJava, pluginDir, clustersize.asJava, sparklerVersion, jcrawl, e)

    val tson = gson.toJson(task)
    val cid = availableparams("crawlid")
    val ts = LocalDateTime.now()
    val encode = Base64.getEncoder.encodeToString(tson.getBytes(StandardCharsets.UTF_8))
    spark.sql(s"""insert into warehouse.crawl_queue values("$agentName", "$cid", "$encode", "$ts", "$formulary")""")
    println("crawl queued")
    "Queued"
  }
  def cronSchedule(e :Events, c: Option[Crawl], availableparams: Map[String,String],
                   injectargs: Option[Seq[String]], args: Seq[String], pluginDir: String, crawlsize: Int, schedule: String): String ={
    val l = List("type:notebook", "notebook:/Shared/System/jobidgenerator", "crawlagent:" + availableparams("idname"))
    val tasks: Map[String, List[String]] = if (injectargs.isDefined) {
      Map("setup" -> l, "inject" -> injectargs.get.toList, "crawl" -> args.toList)
    } else {
      Map("setup" -> l, "crawl" -> args.toList)
    }
    val clustersize: Map[String, Int] = if (injectargs.isDefined) {
      Map("setup" -> 1, "inject" -> 1, "crawl" -> crawlsize)
    } else {
      Map("setup" -> 1, "crawl" -> crawlsize)
    }

    val runid = JobAPI.generateTasksPost(availableparams("crawlid"), tasks, pluginDir, clustersize, sparklerVersion,
      c.get.config("clustertype"), c.get.config("scalaversion"), c.get.config("cpus"),
      c.get.config("drivermemory"), c.get.config("executormemory"), transformTemplate = e, schedule= schedule, idname = "/dbfs/FileStore/seeds/doccrawl_id_" + availableparams("idname"))
    //val rid = u.extractRunID(runid)
    //n.logCrawlStart(availableparams("crawlid"), c.get.config.toMap, c.get.config("clustertype"), c.get.config("clustersize").toInt, 0, rid)
    //n.logAdditionalCrawlEvent(availableparams("crawlid"), "JOB RUN ID", runid)
    "Scheduled: " + runid
  }


}
