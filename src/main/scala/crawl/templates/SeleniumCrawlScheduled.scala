// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl.templates

import crawl.{Utils, WebCrawler}
import management.jobutils.{JobAPI, TasksHolder}
import management.{Crawl, JavaCrawl}

import com.google.gson.Gson
import com.kytheralabs.crawl.databricksapi.templates.Events
import org.apache.spark.sql.SparkSession

import java.nio.charset.StandardCharsets
import java.time.LocalDateTime
import java.util.Base64
import scala.collection.mutable
import scala.collection.JavaConverters._

class SeleniumCrawlScheduled(spark: SparkSession, agentName: String, sparklerVersion: String = "0.5.14", schedule : String = "")
  extends SeleniumCrawl(spark = spark, sparklerVersion = sparklerVersion, agentName = agentName) {


  /**
   * Run a production crawl
   *
   * @param availableparams the parameters available in the crawl
   * @param c               the crawl config object
   * @param injectargs      the args to inject
   * @return
   */
  override def runProductionCrawl(availableparams: Map[String, String], c: Option[Crawl], injectargs: Option[Seq[String]], crawldir: String): String = {


    println("Configuring post crawl transform")
    val webCrawler = new WebCrawler("local", false, sparklerPath = sparklerpath)
    val e = generateTransformTemplate(availableparams, c, crawldir)
    val crawlsize = setProdOptions(webCrawler, availableparams, c).toString
    val args = webCrawler.getCrawlArgs(availableparams("crawlid"), "production", None)

    println("Calling submit job")
    var sparklermainver = getSparklerVersion
    if (getSparklerVersion.contains("-")) {
      val splitpos = getSparklerVersion.lastIndexOf("-")
      sparklermainver = getSparklerVersion.substring(0, splitpos)
    }
    val pluginDir = "/dbfs/FileStore/release/plugins/plugins-" + sparklermainver

    val formulary = c.get.config("formularyid")

    if(schedule == null || schedule == ""){
      onetimequeue(e, c, availableparams, formulary, pluginDir, injectargs, args, crawlsize)
    } else{
      cronSchedule(e, c, availableparams, injectargs, args, pluginDir, crawlsize.toInt, schedule)
    }

  }

  def onetimequeue(e :Events, c: Option[Crawl], availableparams: Map[String,String], formulary: String,
                   pluginDir: String, injectargs: Option[Seq[String]], args: Seq[String], crawlsize: String): String ={
    val tasks: mutable.HashMap[String, java.util.List[String]] = if (injectargs.isDefined) {
      mutable.HashMap("inject" -> injectargs.get.toList.asJava, "crawl" -> args.toList.asJava)
    } else {
      mutable.HashMap("crawl" -> args.toList.asJava)
    }
    val clustersize: mutable.HashMap[String, String] = if (injectargs.isDefined) {
      mutable.HashMap("inject" -> "1", "crawl" -> crawlsize)
    } else {
      mutable.HashMap("crawl" -> crawlsize)
    }
    val gson = new Gson()
    val jcrawl = JavaCrawl(c.get.name, c.get.path, c.get.config.asJava, c.get.transformPath.get, c.get.state)
    val task = TasksHolder(availableparams("crawlid"), formulary, tasks.asJava, pluginDir, clustersize.asJava, getSparklerVersion, jcrawl, e)

    val tson = gson.toJson(task)
    val cid = availableparams("crawlid")
    val ts = LocalDateTime.now()
    val encode = Base64.getEncoder.encodeToString(tson.getBytes(StandardCharsets.UTF_8))
    spark.sql(s"""insert into warehouse.crawl_queue values("$agentName", "$cid", "$encode", "$ts", "$formulary")""")
    println("crawl queued")
    "Queued"
  }

  def cronSchedule(e :Events, c: Option[Crawl], availableparams: Map[String,String],
                   injectargs: Option[Seq[String]], args: Seq[String], pluginDir: String, crawlsize: Int, schedule: String): String ={
    val tasks: Map[String, List[String]] = if (injectargs.isDefined) {
      Map("inject" -> injectargs.get.toList, "crawl" -> args.toList)
    } else {
      Map("crawl" -> args.toList)
    }
    val clustersize: Map[String, Int] = if (injectargs.isDefined) {
      Map("inject" -> 1, "crawl" -> crawlsize)
    } else {
      Map("crawl" -> crawlsize)
    }
    val u = new Utils
    val runid = JobAPI.generateTasksPost(availableparams("crawlid"), tasks, pluginDir, clustersize, sparklerVersion,
      c.get.config("clustertype"), c.get.config("scalaversion"), c.get.config("cpus"),
      c.get.config("drivermemory"), c.get.config("executormemory"), transformTemplate = e, schedule= schedule)
    //val rid = u.extractRunID(runid)
    //n.logCrawlStart(availableparams("crawlid"), c.get.config.toMap, c.get.config("clustertype"), c.get.config("clustersize").toInt, 0, rid)
    //n.logAdditionalCrawlEvent(availableparams("crawlid"), "JOB RUN ID", runid)
    "Scheduled " + runid
  }

}
