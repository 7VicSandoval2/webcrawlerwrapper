// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl.templates

import crawl.databricksapi.templates.{Events, Shutdown, Triggerjob}
import crawl.templates.SeleniumCrawl.defaultNotebookParams
import crawl.{Helper, Utils, WebCrawler}
import management.exceptions.FrameworkException
import management.jobutils.JobAPI
import management.{Crawl, CrawlLogging, GenericLogger}

import com.databricks.dbutils_v1.DBUtilsHolder.dbutils
import com.databricks.dbutils_v1.InputWidgetNotDefined
import management.logger.LogHelper

import org.apache.spark.sql.SparkSession

import scala.collection.JavaConversions._
import scala.language.postfixOps
/**
 * Core Template extendable crawl template
 * @param spark The spark object
 * @param sparklerVersion The required Sparkler Version
 */
class CoreTemplate(spark: SparkSession, sparklerVersion: String) extends ICrawl with LogHelper {
  import spark.implicits._

  val sparklerpath: String = "/dbfs/FileStore/release/sparkler-app-" + sparklerVersion + ".zip"
  var submitpath: String = "dbfs:/FileStore/release/sparkler-app-" + sparklerVersion + ".jar"
  var n : CrawlLogging = new GenericLogger(spark)
  var baseUrlPassed: String = ""
  val genericLogger = new GenericLogger(spark)
  /**
   * Initialise the crawl
   */
  def initCrawl(): Unit ={
    log.info("BASEURL: " + Helper.getBaseUrl())

    try {
      baseUrlPassed = dbutils.widgets.get("baseurl")
      log.debug("Base URL passed: " + baseUrlPassed)
    }
    catch {
      case _ : InputWidgetNotDefined => log.info("\"baseurl\" is not passed as notebook parameter")
    }

    checkPluginVersion
    defaultNotebookParams()
  }

  override def getSparklerVersion: String = {
    if(sparklerVersion == ""){
      dbutils.secrets.get("crawlconfig", "sparklerversion")
    } else{
      sparklerVersion
    }
  }
  /**
   * Finish the crawl
   */
  def finish(): Unit ={
    dbutils.notebook.exit("success")
  }

  /**
   * Get the crawl params
   * @return
   */
  def getParams: Map[String, String] = {
    Map("crawlid" -> dbutils.widgets.get("crawlid"), "token" -> dbutils.widgets.get("token"),
      "formularyid" -> dbutils.widgets.get("formularyid"), "executiontype" -> dbutils.widgets.get("executiontype"),
      "baseurl" -> baseUrlPassed)
  }

  /**
   * Get the tokens
   * @param availableparams the crawl parameters
   * @return
   */
  def getTokens(availableparams:Map[String,String]): Array[String] ={
    try {
      var extractedtokens = ""

      val tokens = if (availableparams("token").startsWith("file://")) {
        extractedtokens = JobAPI.fileToCleanList(availableparams("token").stripPrefix("file://"))
        extractedtokens.split(",")
      } else if (availableparams("token") == "") {
        val url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vReZOCFONO5xMxiRjxSHoP1TbggzVZPmsyIc8mEc73Cc4niVdyabAgFHNQYJp8mErFMUuufUnL7K_IX/pub?gid=1882764027&single=true&output=csv"

        // Fetch the test criteria from Google Docs
        val fileName = "tokenList.csv"
        val u = new Utils
        val df = u.parseGoogleSheet(spark, fileName, url)
        df.select("Value").filter(df("TokenTypeId") === 3).map(r => r.getString(0)).collect
      } else {
        availableparams("token").split(",")
      }
      tokens
    } catch{
      case e : Exception =>
        genericLogger.logCrawlEnd(dbutils.widgets.get("crawlid"), success = false)
        genericLogger.logAdditionalCrawlEvent(dbutils.widgets.get("crawlid"), "Crawl Failed", "No tokens found")
        throw FrameworkException("Error occurred. No tokens found", e)
    }
  }

  /**
   * Get the Jcode tokens
   * @param availableparams the crawl parameters
   * @return
   */
  def getJCodeTokens(availableparams:Map[String,String]) : Array[String] = {

    try {
      var extractedtokens = ""

      val tokens = if (availableparams("token").startsWith("file://")) {
        extractedtokens = JobAPI.fileToCleanList(availableparams("token").stripPrefix("file://"))
        extractedtokens.split(",")
      } else if (availableparams("token") == "") {
        val url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQW-aM0DNL7TDLZFYzFgAp8ZA7opvscDjoAWLtUpIkCANqlU4Dgf_HRezmO0BY5iEpiS_CUrO7kiyzV/pub?gid=1245936838&single=true&output=csv"

        // Fetch the test criteria from Google Docs
        val fileName = "jcodes.csv"
        val u = new Utils
        val df = u.parseGoogleSheet(spark, fileName, url)
        df.select("jcode").map(r => r.getString(0)).collect
      } else {
        availableparams("token").split(",")
      }
      tokens
    } catch {
      case e : Exception =>
        genericLogger.logCrawlEnd(dbutils.widgets.get("crawlid"), success = false)
        genericLogger.logAdditionalCrawlEvent(dbutils.widgets.get("crawlid"), "Crawl Failed", "No tokens found")
        throw FrameworkException("Error occurred. No tokens found", e)
    }
  }

  /**
   * Check the mandatory parameters.
   * @param availableparams the parameters available in this crawl.
   */
  def checkParams(availableparams: Map[String, String]): Unit ={
    if(availableparams("crawlid") == ""){
      throw new Exception("No CrawlID found.")
    }
  }

  /**
   * Generate a transform template
   * @param availableparams the parameters available
   * @param c the crawl config object
   * @return
   */
  def generateTransformTemplate(availableparams: Map[String,String], c : Option[Crawl], subdir: String): Events = {
    val e = new Events
    val shutdown = new Shutdown
    val tj = new Triggerjob

    log.debug("Detecting base url")
    val baseUrl = if(availableparams.contains("baseurl") && availableparams("baseurl") != null && availableparams("baseurl") != "") {
      log.debug("Param set to:" + availableparams("baseurl"))
      availableparams("baseurl")
    }
    else {
      log.info("Fetching base url for subdir: " + subdir)
      log.info("Baseurl is: " + Helper.getBaseUrl(subdir))
      Helper.getBaseUrl(subdir)
    }

    val limportpath = if(availableparams.contains("legacyimportpath") && availableparams("legacyimportpath") != null &&
      availableparams("legacyimportpath") != "") {
      log.info("Param set to:" + availableparams("legacyimportpath"))
      availableparams("legacyimportpath")
    }
    else {
      ""
    }
    val limport = if(availableparams.contains("legacyimport") && availableparams("legacyimport") != null &&
      availableparams("legacyimport") != "") {
      log.info("Param set to:" + availableparams("legacyimport"))
      availableparams("legacyimport")
    }
    else {
      ""
    }
    log.info("Setting transformation path to: " + baseUrl)
    val idname = if(availableparams.contains("idname") && availableparams("idname") != null && availableparams("idname") != "") {
      println("Param set to:" + availableparams("idname"))
      availableparams("idname")
    }
    else {
      ""
    }
    println("Setting transformation path to: "+baseUrl)
    tj.setApiKey(dbutils.secrets.get("crawlconfig", "apikey"))
    tj.setApiURL(dbutils.secrets.get("crawlconfig", "apiurl"))
    tj.setNotebook(baseUrl + "/" + c.get.transformPath.get)
    tj.setSparkversion("8.3.x-scala2.12")
    tj.setInstancetype(c.get.config("clustertype"))
    tj.setClustersize(3)
    val transformparams = Map("agentname" -> c.get.config("agentname"), "crawlid" -> availableparams("crawlid"),
      "debug" -> "false", "formularyid" -> availableparams("formularyid"), "qa" -> "false", "tokentype" -> "3",
      "baseurl" -> baseUrl, "legacyimportpath" -> limportpath, "legacyimport" -> limport, "idname" -> idname)
    val jmap = new java.util.HashMap[String, String](transformparams)
    tj.setParameters(jmap)
    shutdown.setTriggerjob(tj)
    e.setShutdown(shutdown)
    e
  }

  override def getPluginDir: String = {
    var sparklermainver = getSparklerVersion
    if(sparklermainver.contains("-")) {
      val splitpos = sparklermainver.lastIndexOf("-")
      sparklermainver = sparklermainver.substring(0, splitpos)
    }
    "/dbfs/FileStore/release/plugins/plugins-" + sparklermainver
  }

  override def checkPluginVersion(): Unit = {
    log.info("Sparkler Version: " + getSparklerVersion)
    if(Helper.getBaseUrl().contains("/Shared") && getSparklerVersion.contains("SNAPSHOT")){
      log.info("Throwing Exception, cannot run.")
      throw new Exception("Cannot run SNAPSHOT Sparkler build from shared directory")
    }
  }

}
