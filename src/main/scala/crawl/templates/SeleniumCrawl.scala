// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl.templates

import crawl.{Stats, Utils, WebCrawler}
import management.jobutils.JobAPI
import management.{Crawl, GenericLogger, ManagementCore, ScreenLogger}

import com.databricks.dbutils_v1.DBUtilsHolder.dbutils
import com.google.gson.JsonParser
import org.apache.spark.sql.SparkSession


/**
 * Run a selenium based crawl
 *
 * @param spark           the notebook spark object
 * @param agentName       the agent name
 * @param sparklerVersion the sparkler version
 */
class SeleniumCrawl(spark: SparkSession, agentName: String,
                    sparklerVersion: String)

  extends CoreTemplate(spark = spark, sparklerVersion = sparklerVersion) {

  import spark.implicits._

  val stats = new Stats()

  /**
   * Run a selenium based jcode crawl
   *
   * @param url            the url to crawl
   * @param json           the selenium script
   * @param format         the format of the script
   * @param injectonly     inject only or run a full crawl
   * @param disableLogging disable logging
   * @return
   */
  def runJcrawl(url: String, json: String, format: String = "json", injectonly: Boolean = false, disableLogging: Boolean = false): String = {
    val availableparams = getParams
    val tokenlist = getJCodeTokens(availableparams)

    executeAgnosticCrawl(url, json, format, injectonly, disableLogging, tokenlist, "Jcode")
  }

  /**
   * Run a formulary crawl
   *
   * @param url            the url to crawl
   * @param json           the selenium script
   * @param format         the format of the script
   * @param injectonly     inject only or run a full crawl
   * @param disableLogging disable logging
   * @return
   */
  def runCrawl(url: String, json: String, format: String = "json", injectonly: Boolean = false, disableLogging: Boolean = false): String = {
    val availableparams = getParams
    val tokenlist = getTokens(availableparams)

    executeAgnosticCrawl(url, json, format, injectonly, disableLogging, tokenlist, "Crawls")
  }

  /**
   * Execute an agnostic crawl
   *
   * @param url            the url to crawl
   * @param json           the selenium script
   * @param format         the format of the script
   * @param injectonly     inject only or run a full crawl
   * @param disableLogging disable logging
   * @param tokenlist      the token list for the full crawl
   * @return
   */
  private def executeAgnosticCrawl(url: String, json: String, format: String, injectonly: Boolean,
                                   disableLogging: Boolean = false, tokenlist: Array[String], crawldir: String): String = {
    val u = new Utils
    val available = u.checkForSparkler(getSparklerVersion)
    if (!available._1) {
      available._2
    } else {
      if (disableLogging) {
        n = new ScreenLogger()
      } else {
        n = new GenericLogger(spark)
      }
      val availableparams = getParams

      checkParams(availableparams)

      n.bulkUpdateTokenLog(availableparams("crawlid"), tokenlist.toList, "Scraping")

      val injectargs = initCrawl(List(url), json, tokenlist, availableparams, format)
      if (!injectonly) {
        runCrawl(availableparams, injectargs, crawldir)
      } else {
        "Inject Only Job"
      }
    }
  }

  /**
   * Run a multiurl formulary crawl
   *
   * @param url            the url list to crawl
   * @param json           the selenium script
   * @param format         the format of the crawl
   * @param injectonly     inject only or run a full crawl
   * @param disableLogging disable or enable logging
   * @return
   */
  def runMultiCrawl(url: List[String], json: String, format: String = "json", injectonly: Boolean = false, disableLogging: Boolean = false): String = {

    val u = new Utils
    val available = u.checkForSparkler(getSparklerVersion)
    if (!available._1) {
      available._2
    } else {
      if (disableLogging) {
        n = new ScreenLogger()
      } else {
        n = new GenericLogger(spark)
      }
      val availableparams = getParams

      checkParams(availableparams)

      val tokenlist = getTokens(availableparams)

      n.bulkUpdateTokenLog(availableparams("crawlid"), tokenlist.toList, "Scraping")

      val injectargs = initCrawl(url, json, tokenlist, availableparams, format)
      if (!injectonly) {
        runCrawl(availableparams, injectargs, "Crawls")
      } else {
        "Inject Only Job"
      }
    }
  }

  /**
   * Run a multiurl formulary crawl
   *
   * @param url            the url list to crawl
   * @param json           the selenium script
   * @param format         the format of the crawl
   * @param injectonly     inject only or run a full crawl
   * @param disableLogging disable or enable logging
   * @return
   */
  def runMultiJCrawl(url: List[String], json: String, format: String = "json", injectonly: Boolean = false, disableLogging: Boolean = false): String = {

    val u = new Utils
    val available = u.checkForSparkler(getSparklerVersion)
    if (!available._1) {
      available._2
    } else {
      if (disableLogging) {
        n = new ScreenLogger()
      } else {
        n = new GenericLogger(spark)
      }
      val availableparams = getParams

      checkParams(availableparams)

      val tokenlist = getJCodeTokens(availableparams)

      n.bulkUpdateTokenLog(availableparams("crawlid"), tokenlist.toList, "Scraping")

      val injectargs = initCrawl(url, json, tokenlist, availableparams, format)
      if (!injectonly) {
        runCrawl(availableparams, injectargs, "Jcode")
      } else {
        "Inject Only Job"
      }
    }
  }

  /**
   * Initialise a crawl
   *
   * @param url             the url list to crawl
   * @param json            the selenium script
   * @param extractedtokens the tokens
   * @param availableparams the parameters available in the crawl
   * @param format          the format of the crawl
   * @return
   */
  private def initCrawl(url: List[String], json: String, extractedtokens: Array[String],
                        availableparams: Map[String, String], format: String): Option[Seq[String]] = {
    val inputDataframeOfURLS = url.toDF("DOMAIN")
    val u = new Utils
    val install = if (availableparams("executiontype") == "Development") {
      true
    } else {
      false
    }
    val webCrawler = new WebCrawler("local", install, sparklerPath = sparklerpath)
    if (format == "json") {
      webCrawler.setFetcherSeleniumScript(new JsonParser().parse(json).getAsJsonObject)
    } else {
      log.info("Converting yaml to json")
      val o = u.yamltojson(json)
      webCrawler.setFetcherSeleniumScript(new JsonParser().parse(o).getAsJsonObject)
    }
    webCrawler.setFetcherChrome()
    webCrawler.setFetcherValues(extractedtokens.toList.toDF())
    if (availableparams("executiontype") == "Development") {
      webCrawler.inject(availableparams("crawlid"), inputDataframeOfURLS.select('DOMAIN))
      None
    } else {
      val seeds: List[String] = inputDataframeOfURLS.select('DOMAIN).map(r => r.getString(0)).collect.toList
      u.writeFile("/dbfs/FileStore/seeds/" + availableparams("crawlid") + "seeds.txt", seeds)

      val seedfile = "/dbfs/FileStore/seeds/" + availableparams("crawlid") + "seeds.txt"
      val args = webCrawler.getInjectArgs(availableparams("crawlid"), inputDataframeOfURLS.select('DOMAIN), seedfile, configtofile = true)
      Some(args)
    }
  }


  /**
   * Run a crawl
   *
   * @param availableparams the parameters available in the crawl
   * @param injectargs      the arguments to inject
   * @return
   */
  private def runCrawl(availableparams: Map[String, String], injectargs: Option[Seq[String]], crawldir: String): String = {
    log.info("Running Crawl")
    if (availableparams("executiontype") == "Development") {
      runDevelopmentCrawl(availableparams)
    } else {
      //run spark submit
      log.info("Launching in production mode")
      val m = new ManagementCore(spark)
      val c = if (crawldir == "Crawls" || crawldir == "") {
        m.getCrawl(agentName + "_" + dbutils.widgets.get("formularyid"))
      } else {
        m.getJCrawl(agentName + "_" + dbutils.widgets.get("formularyid"))
      }
      if (c.isEmpty) {
        log.info("Failed, no config found")
        throw new Exception("Failed, crawl not found in the database")
        "Failed, crawl not found in the database"
      } else {
        runProductionCrawl(availableparams, c, injectargs, crawldir)
      }
    }
  }

  /**
   * Run a development crawl
   *
   * @param availableparams the parameters available in the crawl
   * @return
   */
  def runDevelopmentCrawl(availableparams: Map[String, String]): String = {
    val webCrawler = new WebCrawler("local", true, sparklerPath = sparklerpath)
    webCrawler.setChromeOutputLocation("/dbfs/FileStore/crawl/content/")
    webCrawler.setFetcherChrome()
    log.info("Launching in development mode: " + webCrawler.getCrawlArgs(availableparams("crawlid")))
    val i = webCrawler.crawl(availableparams("crawlid"))
    i.toString
  }

  /**
   * Run a production crawl
   *
   * @param availableparams the parameters available in the crawl
   * @param c               the crawl config object
   * @param injectargs      the args to inject
   * @return
   */
  def runProductionCrawl(availableparams: Map[String, String], c: Option[Crawl], injectargs: Option[Seq[String]], crawldir: String): String = {
    log.info("Configuring post crawl transform")
    val webCrawler = new WebCrawler("local", false, sparklerPath = sparklerpath)
    val e = generateTransformTemplate(availableparams, c, crawldir)
    val crawlsize = setProdOptions(webCrawler, availableparams, c)
    val args = webCrawler.getCrawlArgs(availableparams("crawlid"), "production", None)

    log.info("Calling submit job")

    val tasks: Map[String, List[String]] = if (injectargs.isDefined) {
      Map("inject" -> injectargs.get.toList, "crawl" -> args.toList)
    } else {
      Map("crawl" -> args.toList)
    }

    val clustersize: Map[String, Int] = if (injectargs.isDefined) {
      Map("inject" -> 1, "crawl" -> crawlsize)
    } else {
      Map("crawl" -> crawlsize)
    }

    val u = new Utils
    val runid = JobAPI.generateTasksPost(availableparams("crawlid"), tasks, getPluginDir, clustersize, getSparklerVersion,
      c.get.config("clustertype"), c.get.config("scalaversion"), c.get.config("cpus"),
      c.get.config("drivermemory"), c.get.config("executormemory"), transformTemplate = e)
    val rid = u.extractRunID(runid)
    n.logCrawlStart(availableparams("crawlid"), c.get.config.toMap, c.get.config("clustertype"), c.get.config("clustersize").toInt, 0, rid)
    n.logAdditionalCrawlEvent(availableparams("crawlid"), "JOB RUN ID", runid)
    runid
  }

  /**
   * Set the production options in the webcrawler object
   *
   * @param webCrawler      the webcrawler
   * @param availableparams the parameters available
   * @param c               the crawl config object
   * @return
   */
  def setProdOptions(webCrawler: WebCrawler, availableparams: Map[String, String], c: Option[Crawl]): Int = {
    webCrawler.setChromeOutputLocation("/dbfs/FileStore/crawl/content/")
    webCrawler.setFetcherChrome()
    webCrawler.setSparkMaster("")

    val tokencount = getTokens(availableparams).length
    var clustersize = c.get.config("clustersize").toInt
    log.info("Found tokens: " + tokencount)
    if (tokencount < c.get.config("clustersize").toInt) {
      clustersize = getTokens(availableparams).length
    }
    log.info("Setting clustersize: " + clustersize)
    if ((tokencount / 7) < clustersize) {
      webCrawler.setRepartitionCount(clustersize)
    } else {
      val partitions = tokencount / 7
      if (partitions < clustersize) {
        webCrawler.setRepartitionCount(clustersize)
      } else {
        webCrawler.setRepartitionCount(partitions)
      }
    }
    clustersize
  }
}

/**
 * Selenium Crawl
 */
object SeleniumCrawl {
  /**
   * The default notebook params
   */
  def defaultNotebookParams(): Unit = {
    dbutils.widgets.text("crawlid", "")
    dbutils.widgets.text("formularyid", "")
    dbutils.widgets.text("token", "")
    dbutils.widgets.text("schedule", "")
    dbutils.widgets.dropdown("executiontype", "Development", Seq("Development", "Production"), "Execution Type")
  }
}
