// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl.templates

import crawl.databricksapi.templates.Events
import crawl.{Utils, WebCrawler}
import management.jobutils.{JobAPI, TasksHolder}
import management.{Crawl, JavaCrawl, ManagementCore}

import com.databricks.dbutils_v1.DBUtilsHolder.dbutils
import com.databricks.dbutils_v1.InputWidgetNotDefined
import com.google.gson.Gson
import org.apache.spark.sql.SparkSession

import java.nio.charset.StandardCharsets
import java.time.LocalDateTime
import java.util.Base64
import scala.collection.JavaConverters._


class BareCrawl(spark: SparkSession, agentName: String, sparklerVersion : String = "0.5.14", locdir : String = "Crawls", schedule : String = "")
  extends CoreTemplate(spark = spark, sparklerVersion = sparklerVersion) {
  import spark.implicits._

  dbutils.widgets.text("urls", "")
  dbutils.widgets.text("legacyimportpath", "")
  dbutils.widgets.text("legacyimport", "")

  private def initCrawl(url: List[String], availableparams: Map[String, String]): Option[Seq[String]] = {
    try {
      baseUrlPassed = dbutils.widgets.get("baseurl")
      log.info("Base URL passed: " + baseUrlPassed)
    }
    catch {
      case _ : InputWidgetNotDefined => log.info("\"baseurl\" is not passed as notebook parameter")
    }
    
    val inputDataframeOfURLS = url.toDF("DOMAIN")

    //This is development mode, production needs to do this in sparksubmit
    val webCrawler = new WebCrawler("local", installLocalSparkler = true, sparklerPath = sparklerpath)
    log.info("removing url injector")
    webCrawler.removeUrlInjector()
    webCrawler.fetcherActivePlugins -= "url-injector"
    log.info("done removing url injector")
    webCrawler.fetcherActivePlugins.foreach(f => {
      log.info(f)
    })

    val u = new Utils
    val seeds : List[String] = inputDataframeOfURLS.select('DOMAIN).map(r => r.getString(0)).collect.toList
    u.writeFile("/dbfs/FileStore/seeds/" + availableparams("crawlid") + "seeds.txt", seeds)
    val args = webCrawler.getInjectArgs(availableparams("crawlid"), inputDataframeOfURLS.select('DOMAIN),
      "/dbfs/FileStore/seeds/" + availableparams("crawlid") + "seeds.txt", configtofile = true)
    Some(args)
  }

  def getOurParams: Map[String, String] = {
    Map("crawlid" -> dbutils.widgets.get("crawlid"), "token" -> dbutils.widgets.get("token"),
      "formularyid" -> dbutils.widgets.get("formularyid"), "executiontype" -> dbutils.widgets.get("executiontype"),
      "baseurl" -> baseUrlPassed, "legacyimportpath" -> dbutils.widgets.get("legacyimportpath"), "legacyimport" -> dbutils.widgets.get("legacyimport"))
  }

  def runCrawl(url: List[String], headers: Seq[(String, String)],  injectonly : Boolean = false, tformlibraries: String = "", iterations : Int = 1) : String = {

    var availableparams = getOurParams

    checkParams(availableparams)

    val args = initCrawl(url, availableparams)
    availableparams = getOurParams
    if(!injectonly) {
      executeCrawl(availableparams, headers, locdir, args, tformlibraries, iterations)
    } else{
      "Inject Only Job"
    }
  }

  /**
   * Execute a crawl
   * @param availableparams the parameters available in this crawl
   * @param headers the headers you want to inject
   * @return
   */
  private def executeCrawl(availableparams: Map[String, String], headers: Seq[(String, String)], subdir: String,
                           injectargs: Option[Seq[String]], tformlibraries: String, iterations: Int): String = {
    log.info("Running Crawl")
    if(availableparams("executiontype") == "Development") {
      runDevelopmentCrawl(availableparams, headers, iterations)
    } else {
      //run spark submit
      log.info("Launching in production mode")

      val m = new ManagementCore(spark)
      val c = m.getDocCrawl(agentName + "_" + dbutils.widgets.get("formularyid"))
      if (c.isEmpty) {
        log.info("Failed, no config found")
        "Failed, crawl not found in the database"
      } else {
        runProductionCrawl(availableparams, headers, c, injectargs, subdir, tformlibraries, iterations)
      }
    }
  }

  /**
   * Run a Development crawl
   * @param availableparams the parameters available in this crawl
   * @param headers the headers you want to inject
   * @return
   */
  private def runDevelopmentCrawl(availableparams: Map[String, String], headers: Seq[(String, String)], iterations: Int): String ={
    val webCrawler = new WebCrawler( "local", true, sparklerPath = sparklerpath)
    webCrawler.setFetcherHeaders(headers)

    log.info("Launching in development mode")
    val i = webCrawler.crawl(availableparams("crawlid"), iterations)
    i.toString
  }

  /**
   * Run a production crawl
   * @param availableparams the parameters available in this crawl
   * @param headers the headers you want to inject
   * @param c the config option
   * @return
   */
  private def runProductionCrawl(availableparams: Map[String, String], headers: Seq[(String, String)], c: Option[Crawl],
                                 injectargs: Option[Seq[String]], subdir: String, tformlibraries: String, iterations: Int): String ={
    val webCrawler = new WebCrawler("local", false, sparklerPath = sparklerpath)

    log.info("Configuring post crawl transform")
    log.info("subdir is: " + subdir)
    val e = generateTransformTemplate(availableparams, c, subdir)

    val repartitionCount = 500
    val crawlW = 100000
    webCrawler.setRepartitionCount(repartitionCount)
    webCrawler.setRepartitionCount(25)
    webCrawler.setFetcherHeaders(headers)
    webCrawler.crawlWidth = crawlW
    webCrawler.contentFileName="hash"
    webCrawler.crawlDepth = iterations
    webCrawler.setSparkMaster("")
    val args = webCrawler.getCrawlArgs(availableparams("crawlid"), "production", None)

    log.info("Calling submit job")


    val crawlsize = c.get.config("clustersize")


    val formulary = c.get.config("formularyid")
    var sparklermainver = sparklerVersion
    if (sparklerVersion.contains("-")) {
      val splitpos = sparklerVersion.lastIndexOf("-")
      sparklermainver = sparklerVersion.substring(0, splitpos)
    }
    val pluginDir = "/dbfs/FileStore/release/plugins/plugins-" + sparklermainver

    if(schedule == null || schedule == ""){
      onetimequeue(e, c, availableparams, formulary, pluginDir, injectargs, args, crawlsize)
    } else{
      cronSchedule(e, c, availableparams, injectargs, args, pluginDir, crawlsize.toInt, schedule)
    }
  }
  def onetimequeue(e :Events, c: Option[Crawl], availableparams: Map[String,String], formulary: String,
                   pluginDir: String, injectargs: Option[Seq[String]], args: Seq[String], crawlsize: String): String = {
    val tasks: Map[String, java.util.List[String]] = if (injectargs.isDefined) {
      Map("inject" -> injectargs.get.toList.asJava, "crawl" -> args.toList.asJava)
    } else {
      Map("crawl" -> args.toList.asJava)
    }
    val clustersize: Map[String, String] = if (injectargs.isDefined) {
      Map("inject" -> "1", "crawl" -> crawlsize)
    } else {
      Map("crawl" -> crawlsize)
    }
    val gson = new Gson()
    val jcrawl = JavaCrawl(c.get.name, c.get.path, c.get.config.asJava, c.get.transformPath.get, c.get.state)
    val task = TasksHolder(availableparams("crawlid"), formulary, tasks.asJava, getPluginDir, clustersize.asJava, getSparklerVersion, jcrawl, e)

    val tson = gson.toJson(task)
    val cid = availableparams("crawlid")
    val ts = LocalDateTime.now()
    val encode = Base64.getEncoder.encodeToString(tson.getBytes(StandardCharsets.UTF_8))
    spark.sql(s"""insert into warehouse.crawl_queue values("$agentName", "$cid", "$encode", "$ts", "$formulary")""")
    log.info("crawl queued")
    "Queued"
  }
  def cronSchedule(e :Events, c: Option[Crawl], availableparams: Map[String,String],
                   injectargs: Option[Seq[String]], args: Seq[String], pluginDir: String, crawlsize: Int, schedule: String): String ={
    val tasks: Map[String, List[String]] = if (injectargs.isDefined) {
      Map("inject" -> injectargs.get.toList, "crawl" -> args.toList)
    } else {
      Map("crawl" -> args.toList)
    }
    val clustersize: Map[String, Int] = if (injectargs.isDefined) {
      Map("inject" -> 1, "crawl" -> crawlsize)
    } else {
      Map("crawl" -> crawlsize)
    }

    val u = new Utils
    val runid = JobAPI.generateTasksPost(availableparams("crawlid"), tasks, pluginDir, clustersize, sparklerVersion,
      c.get.config("clustertype"), c.get.config("scalaversion"), c.get.config("cpus"),
      c.get.config("drivermemory"), c.get.config("executormemory"), transformTemplate = e, schedule= schedule)
    //val rid = u.extractRunID(runid)
    //n.logCrawlStart(availableparams("crawlid"), c.get.config.toMap, c.get.config("clustertype"), c.get.config("clustersize").toInt, 0, rid)
    //n.logAdditionalCrawlEvent(availableparams("crawlid"), "JOB RUN ID", runid)
    "Scheduled: " + runid
  }
}
