// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl.templates

import crawl.databricksapi.templates.Events
import management.Crawl

import com.kytheralabs.crawl.WebCrawler

trait ICrawl {
  def initCrawl() : Unit

  def finish() : Unit

  def getParams: Map[String, String]

  def getTokens(availableparams:Map[String,String]): Array[String]

  def getJCodeTokens(availableparams:Map[String,String]) : Array[String]

  def checkParams(availableparams: Map[String, String]): Unit

  def generateTransformTemplate(availableparams: Map[String,String], c : Option[Crawl], subdir: String): Events

  def getSparklerVersion : String

  def getPluginDir : String

  def checkPluginVersion() : Unit

}
